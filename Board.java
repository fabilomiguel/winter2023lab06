public class Board
{
	private boolean[] tiles;
	private Die die1;
	private Die die2;

	//constructor of the board class, with an array of booleans and two dice.
	public Board() 
	{
		this.tiles = new boolean[12];
		this.die1 = new Die();
		this.die2 = new Die();
	}
	
	
	public String toString()
	{
		String tilesBoard = " ";
		
		for (int i=0;  i < tiles.length; i++)
		{
			if (tiles[i] == false)
			{
				tilesBoard = tilesBoard + (i+1)+" " ;
			}
			else
			{
				tilesBoard = tilesBoard + "X ";
			}
			
		}
		return "The value of tiles are: "+ tilesBoard +" ";
	}
	
	public boolean playATurn()
	{
		die1.roll();
		die2.roll();
		
		//Print the face values of both dice
		System.out.println("Die 1 rolled to be a: " + die1.getFaceValue());
		System.out.println("Die 2 rolled to be a: " + die2.getFaceValue());
    
		//Find the tile to shut based on the sum of the both dice
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		
		//First check - case where sum of dice is the tile to shut
		if (sumOfDice <= 12 && sumOfDice >= 2 && !tiles[sumOfDice-1]) 
		{
            tiles[sumOfDice-1] = true;
            System.out.println("Closing tile equal to sum: " + sumOfDice);
            return false;
        } 
		//Second check - case where the value of the first die is the same as the tile to shut
		else if (!tiles[die1.getFaceValue()-1]) 
		{
            tiles[die1.getFaceValue()-1] = true;
            System.out.println("Closing tile with the same value as die one: " + die1.getFaceValue());
            return false;
        }
		//Third check - case where the value of the second die is the same as the tile to shut
		else if (!tiles[die2.getFaceValue()-1]) 
		{
            tiles[die2.getFaceValue()-1] = true;
            System.out.println("Closing tile with the same value as die two: " + die2.getFaceValue());
            return false;
        } else 
		{
            System.out.println("All the tiles for these values are already shut");
            return true;
        }
	}
}
