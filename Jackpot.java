public class Jackpot{
	public static void main(String[] args)
	{
		System.out.println("Welcome to Jackpot, by Fabilo Silva 2235291");		
		Board jackpot = new Board();
		System.out.println(jackpot);
		
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while (gameOver != true)
		{
			System.out.println(jackpot);
			boolean turn = jackpot.playATurn();
			if (turn == true)
			{
				gameOver = true;
			}
			if (turn == false)
			{
				numOfTilesClosed = numOfTilesClosed + 1;
			}
			
		}
		
		if (numOfTilesClosed >= 7)
		{
			System.out.println("You reached Jackpot and are the big winner!");	
		}
		else
		{
			System.out.println("You lost!");	
		}
	}
}