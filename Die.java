import java.util.Random;

public class Die
{
	private int faceValue;
	private Random rolledValue;
	
	//constructor that will set the initial value of the die to 1 and create a random object
	public Die() 
	{
		this.faceValue = 1; 
		this.rolledValue = new Random();
	}

	public int getFaceValue()
	{
		return this.faceValue;
	}

	public void roll() //method that will use the random object and then make a integer to the face value of the object die
	{
		this.faceValue  = this.rolledValue.nextInt(6)+1;;
	}

	public String toString()
	{
		return "The face value of the die is: " +this.faceValue;
	}
}